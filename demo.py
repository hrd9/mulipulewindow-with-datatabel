import datetime
from kivymd.uix.screen import MDScreen
from kivymd.app import MDApp
from kivy.uix.screenmanager import ScreenManager, Screen
import openpyxl
from openpyxl import workbook, load_workbook
from openpyxl.utils import get_column_letter
import time
from kivymd.uix.dialog import  MDDialog
from kivymd.uix.button import  MDFlatButton, MDRectangleFlatButton
from kivymd.uix.datatables import MDDataTable
from kivy.metrics import dp

class FirstWindow(Screen):
    pass
class SecondWindow(Screen):
    def build(self):
        screen = Screen()
        tabel = MDDataTable(
            size_hint=(0.9, 0.5),
            pos_hint={'center_x': 0.5, 'center_y': 0.5},
            check=True,
            column_data=[
                ("First Name", dp(30)),
                ("last Name", dp(30)),
                ("Email Address", dp(30)),
                ("Phone Number", dp(30)),
            ],
            row_data=[("ram", "sharma", "ram@gmail.com", "+91863012")]
        )
        tabel.bind(on_check_press=self.checked)
        tabel.bind(on_row_press=self.row_checked)
        screen.add_widget(tabel)
        return screen

    def checked(self, instance_table, current_row):
        print(instance_table, current_row)

    def row_checked(self, instance_table, instance_row):
        print(instance_table, instance_row)

class WindowManager(ScreenManager):
    pass
class EliyaApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = "Light"
        self.theme_cls.primary_palette = "DeepPurple"
        self.theme_cls.accent_palette = "Red"
EliyaApp() .run()
